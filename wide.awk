BEGIN {
    print "<!DOCTYPE HTML>\n<html><head>\n"
    print "<title>Wide Feed</title>\n"
    # print "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />\n"
    print "</head><body class=\"noframe\"><div id=\"items\"><pre>\n"

    }

{
    formatted = "<h3><a id=\"" $6 "\" href=\"" $3 "\">" $2 "</a></h3>\n" $4 "\n"
    gsub("\\\\n", "\n", formatted);
    gsub("\\\\t", "\t", formatted);
    print formatted
}

END {
    print"</pre></div></body>\n"
    }
